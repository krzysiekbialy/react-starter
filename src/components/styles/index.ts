export {
  TopLeftContainer,
  mainTitle,
  textWrapper,
  bottomTextWrapper,
  leftContent,
  basketAndTextContainer,
  parartaphLeftStyle,
  boxTitle2Style,
  boxTitle3Style,
  goBack
} from "./TopLeftStyles";
export { TopRightContainer } from "./TopRightStyles";
export {
  ImgWrapper,
  LogoWrapper,
  PartnersTitleDiv,
  PartnersParagraph,
  PartnersContainer
} from "./PartnersStyle";
